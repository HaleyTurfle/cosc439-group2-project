This is a kernel module to determine the authorization of USB devices to either blacklist or whitelist the usb-storage module on Linux to effectively disable USB ports.

Steps to replicate:
    - (compile custom kernel, not necessary but was a project requirement)
    - Create list of authorized devices called AUTHORIZEDDEVS.txt 
        - Ensure file is in same directory as usbauth module
        - Formatted ####X####, … (where first hash set = ProdID, second = VendorID)
    - Pull project from git repo
        - Makefile and usbauth.c needed (files need to be within same dir)
        - Run “make” in terminal to compile
    - Set kernel module “usbauth” to run on boot
        - Add to /etc/modules 
            - run "sudo vim /etc/modules" in terminal
            - “i” to enter insert mode
            - add line “usbauth” to file
            - “:wq” to save
        - Add module to drivers
            - mkdir called usbauth in /lib/modules/*version*/kernel/drivers/
            - cp module in newly created directory
            - Then run “sudo depmod” in terminal
            - Then run “lsmod” in terminal to ensure module is inserted in kernel
    - Blacklist usb-storage module
        - run "sudo vim /etc/modprobe.d/blacklist.conf" in terminal
        - “i” to enter insert mode
        - Add line “blacklist usb-storage” to file 
        - “:wq!” To same
        - Reboot and module should be loaded
        - Run “dmesg” in terminal to see kernel module process statements
