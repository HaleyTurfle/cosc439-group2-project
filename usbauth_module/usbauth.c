#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/syscalls.h>
#include <linux/fcntl.h>
#include <asm/uaccess.h>
#include <linux/uaccess.h>
#include <asm/segment.h>
#include <linux/buffer_head.h>
#include <linux/fs.h>
#include <linux/file.h>
#include <linux/version.h>
#include <linux/slab.h>
#include <linux/stat.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Turfle,Wong,Ampofo");
MODULE_DESCRIPTION("Ensure authorization of USB devices to load usb-storage module");

/* citation for adaptation of file_open, file_read, file_write, and file_close methods: https://stackoverflow.com/questions/1184274/read-write-files-within-a-linux-kernel-module */

/*function to open the file and return the struct file *file variable (needed to read/write)*/
struct file *file_open(const char *path, int flags, int rights){
    struct file *filp = NULL;
    mm_segment_t oldfs;
    int err = 0;

    oldfs = get_fs();
    set_fs(KERNEL_DS);
    filp = filp_open(path, flags, rights);
    set_fs(oldfs);
    if (IS_ERR(filp)) {
        err = PTR_ERR(filp);
        return NULL;
    }
    return filp;
}

/*function to close the file*/
void file_close(struct file *file) {
    filp_close(file, NULL);
}

/*function to read the file and return the contents in a buffer variable*/
unsigned char *file_read(struct file *file, unsigned long long offset, unsigned char *data, unsigned int size) {
    mm_segment_t oldfs;
    int ret;
    unsigned char *null;

    oldfs = get_fs();
    set_fs(KERNEL_DS);

    ret = vfs_read(file, data, size, &offset);

    set_fs(oldfs);
    if(ret >= 0){
    	return data;
    }
    else{
    	return null;
    }
}

/*function to write to file */
void file_write(struct file *file, unsigned long long offset, unsigned char *data, unsigned int size) {	
    /*
    mm_segment_t oldfs;
    int ret;

    oldfs = get_fs();
    set_fs(KERNEL_DS);

    ret = vfs_write(file, data, size, &offset);

    file_close(file);

    set_fs(oldfs);
    */
}

/*function to count number of characters in a char array*/
static int chars(unsigned char *string){
    int total = 0;
    int i = 0;
    const char *newstring = (const char *)string;
    while(newstring[i] != (char)0){
	    total++;
	    i++;
    }
    return total;
}

/*citation for adaptation of size_file function: https://stackoverflow.com/questions/18966071/how-do-you-get-the-size-of-a-file-in-the-linux-kernel*/
/*function to return the size of a file*/
static int size_file(const char* filename){
    mm_segment_t fs;
    struct file *fp;
    struct kstat *stat;
    int size = 0;
    int *input_size = &size;
    char *buf;
    int ret = 0;
    loff_t pos = 0;
   
    fp = filp_open(filename,0,0);
    fs = get_fs();
    set_fs(KERNEL_DS); 
    
    stat =(struct kstat *) kmalloc(sizeof(struct kstat), GFP_KERNEL);
    vfs_stat(filename, stat);
    *input_size = stat->size;
    buf = kmalloc(*input_size, GFP_KERNEL);
    ret = vfs_read(fp, buf, *input_size, &pos);

    filp_close(fp, NULL);
    set_fs(fs);
    kfree(stat);
    return *input_size;
}

/* checks to see if usb_storage module is blacklisted, returns bool. checks by determining if blacklist file contains '#usb_storage'-false, 'usb_storage'-true, or none-false*/
static bool check_usbstorageblacklist(const char *blacklistfile){
	bool usbstorageblacklist = false;
	/* search blacklist.conf for usb_storage*/
	struct file *blacklist;
	int flags = 0;
	int rights = 0;
	int i = 0;
	unsigned long long offset = 0;
	unsigned char data[50];
	unsigned int size = sizeof(data);
	unsigned char *blacklistedModules;

	offset = (size_file(blacklistfile) - 23);

	blacklist = file_open(blacklistfile,flags,rights);
	blacklistedModules = file_read(blacklist,offset,data,size);
	printk("%s",blacklistedModules);
	
	while(i < chars(blacklistedModules)){
		if(blacklistedModules[i] == 'b' && blacklistedModules[i+1] == 'l'&& blacklistedModules[i+2] == 'a' && blacklistedModules[i+3] == 'c'&& blacklistedModules[i+4] == 'k' && blacklistedModules[i+5] == 'l' && blacklistedModules[i+6] == 'i' && blacklistedModules[i+7] == 's' && blacklistedModules[i+8] == 't' && blacklistedModules[i+9] == ' ' && blacklistedModules[i+10] == 'u' && blacklistedModules[i+11] == 's' && blacklistedModules[i+12] == 'b' && blacklistedModules[i+13] == '-' && blacklistedModules[i+14] == 's' && blacklistedModules[i+15] == 't' && blacklistedModules[i+16] == 'o' && blacklistedModules[i+17] == 'r' && blacklistedModules[i+18] == 'a' && blacklistedModules[i+19] == 'g' && blacklistedModules[i+20] == 'e'){
			if(blacklistedModules[i-1] != '#'){
				usbstorageblacklist = true;
			}
		}
		i++;
	}

	printk("\nusb-storgae module blacklisted?");
	if(usbstorageblacklist){
		printk("TRUE");
	}
	else{
		printk("FALSE");
	}

	return usbstorageblacklist;
}

/* write a # to beginning of last line in blacklist file */
static void whitelist(const char *blacklistfile){
	struct file *blacklist;
	int flags = 0;
	int rights = 0;
	unsigned long long offset = 0;
	unsigned char *data = "#blacklist usb-storage";
	unsigned int size = sizeof(data);
	offset = (size_file(blacklistfile) - 21);

	blacklist = file_open(blacklistfile,flags,rights);
	file_write(blacklist,offset,data,size);
	
	printk("\nINSTRUCTIONS: WHITELIST usb-storage");
    	printk("\nINSTRUCTIONS: 1.) sudo vim %s", blacklistfile);
    	printk("\nINSTRUCTIONS: 2.) comment out line containing 'blacklist usb-storage' using '#' character");
}

/* ensure blacklist, if not then remove whitelist */
static void blacklist(const char *blacklistfile){
	struct file *blacklist;
        int flags = 0;
        int rights = 0;
        unsigned long long offset = 0;
        unsigned char *data = "blacklist usb-storage";
        unsigned int size = sizeof(data);
        offset = (size_file(blacklistfile) - 22);

	blacklist = file_open(blacklistfile,flags,rights);
	file_write(blacklist,offset,data,size);
		
	printk("\nINSTRUCTIONS: BLACKLIST usb-storage");
    	printk("\nINSTRUCTIONS: 1.) sudo vim %s", blacklistfile);
    	printk("\nINSTRUCTIONS: 2.) remove comment character '#' from line containing '#blacklist usb-storage'");
}


/*compare list of usb devices to list of 'authorized' usb devices by reading 2 files containing those data sets */
static bool authorize(const char *devices, const char *filename, const char *blacklistfile){
	bool authorize = false;

	/*create list of currently connected USB devices*/
	struct file *authdevs;
	unsigned char *authUSBdevices;

	int i = 0;
	int j = 0;
	int k = 0;
	int ids = 0;
	int authids = 0;
	char IDs[100];
	char authIDs[100];

	int flags = 0;
	int rights = 0;
	unsigned long long offset = 0;
	unsigned long long offset0 = 0;
	unsigned char data[100];
	unsigned int size = sizeof(data);
	unsigned long long devicessize = 0;

	devicessize = 5000;
	printk("%lld",devicessize);

	while((offset0 + 250) < devicessize){
		struct file *devs;
		unsigned char *USBdevice;
		int flags0 = 0;
		int rights0 = 0;
		unsigned char data0[250];
		unsigned int size0 = sizeof(data0);
		devs = file_open(devices,flags0,rights0);
		offset0 = (offset0 + 150);
	        USBdevice = file_read(devs,offset0,data0,size0);
		i = 0;
		while(i < chars(USBdevice)){
			if((USBdevice[i] == 'P') && (USBdevice[i+1] == ':')){
                       		IDs[ids] = (USBdevice[i + 11]);
				IDs[ids+1] = (USBdevice[i + 12]);
				IDs[ids+2] = (USBdevice[i + 13]);
				IDs[ids+3] = (USBdevice[i + 14]);
				IDs[ids+4] = ('X');
				IDs[ids+5] = (USBdevice[i + 23]);
			        IDs[ids+6] = (USBdevice[i + 24]);
			        IDs[ids+7] = (USBdevice[i + 25]);
				IDs[ids+8] = (USBdevice[i + 26]);
				IDs[ids+9] = (',');
				ids = (ids + 10);
				i = (i + 26);
			}
			else{
				i++;
			}
		}
		/*file_close(devs);*/
	}

	/*read from /etc/authorizedUSBdevices.txt to compare contents against list of connected USB devices*/			
	authdevs = file_open(filename,flags,rights);
	authUSBdevices = file_read(authdevs,offset,data,size);	
	printk(authUSBdevices);
	while((j < chars(authUSBdevices) && (j < 100))){
		authIDs[authids] = (authUSBdevices[j + 0]);
		authIDs[authids+1] = (authUSBdevices[j + 1]);
		authIDs[authids+2] = (authUSBdevices[j + 2]);
		authIDs[authids+3] = (authUSBdevices[j + 3]);
		authIDs[authids+4] = (authUSBdevices[j + 4]);
		authIDs[authids+5] = (authUSBdevices[j + 5]);
		authIDs[authids+6] = (authUSBdevices[j + 6]);
		authIDs[authids+7] = (authUSBdevices[j + 7]);
		authIDs[authids+8] = (authUSBdevices[j + 8]);
		authIDs[authids+9] = (authUSBdevices[j + 9]);
		authids = (authids + 10);
		j = (j+10);
	}
	file_close(authdevs);

	printk("\nIDs: %s",IDs);
	printk("\nauthIDs: %s",authIDs);

	/*format char * of data and loop through them to compare*/
	while(k < ids){	
		char searchID[10];
		memcpy(searchID, &IDs[k], 10);
		searchID[9] = '\0';
		printk("DEBUG: comparison");
		printk("\nsearching for :%s",searchID);
		printk("\nwithin %s", authIDs);
		if(strstr(authIDs,searchID) != NULL){
			authorize = true;
		}
		else{
			authorize = false;
			break;
		}
		k = (k + 10);
	}
	return authorize;
}

static int __init init(void){
	bool blacklisted = false;
	bool remblacklist = false;
	printk("\n\n\n********************usbauth start********************\n\n\n");
	printk(KERN_INFO "\nENSURING USB_STORAGE BLACKLIST ... \n");
        blacklisted = check_usbstorageblacklist("/etc/modprobe.d/blacklist.conf");
	printk("\nBEGINNING USB AUTHORIZATION CHECK ... \n");

	remblacklist = authorize("/sys/kernel/debug/usb/devices","AUTHORIZEDDEVS.txt","/etc/modprobe.d/blacklist.conf");
	if(!blacklisted && remblacklist){
		printk("\n ************************* \nALL DEVICES WERE AUTHORIZED, USB CONNECTION SWITCHING TO ENABLED.\n ************************* \n");
		whitelist("/etc/modprobe.d/blacklist.conf");
	}
	else{
		blacklist("/etc/modprobe.d/blacklist.conf");
		printk("\n ************************* \nNOT ALL DEVICES WERE AUTHORIZED, USB CONNECTION DISABLED.\n ************************* \n");
	}
	printk("\n\n\n********************usbauth end********************\n\n\n");
	return 0;
}

static void __exit cleanup(void){
	printk(KERN_INFO "\nUSB AUTHORIZATION CHECK COMPLETE.\n");
}

module_init(init);
module_exit(cleanup);
